﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Guajira.Utilidades
{
    public static class Alertas
    {
        public static string EXITO(string mensaje)
        {
            var html = "<div class=\"alert alert-success alert-dismissible\" role=\"alert\">";
            html += "<button type = \"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
            html += "<strong>Mensaje!</strong> " + mensaje + "</div>";
            return html;
        }

        public static string INFORMACION(string mensaje)
        {
            var html = "<div class=\"alert alert-info alert-dismissible\" role=\"alert\">";
            html += "<button type = \"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
            html += "<strong>Mensaje!</strong> " + mensaje + "</div>";
            return html;
        }

        public static string ERROR(string mensaje)
        {
            var html = "<div class=\"alert alert-danger alert-dismissible\" role=\"alert\">";
            html += "<button type = \"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
            html += "<strong>Mensaje!</strong> " + mensaje + "</div>";
            return html;
        }

        public static string ADVERNTECIA(string mensaje)
        {
            var html = "<div class=\"alert alert-warning alert-dismissible\" role=\"alert\">";
            html += "<button type = \"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
            html += "<strong>Mensaje!</strong> " + mensaje + "</div>";
            return html;
        }
    }
}