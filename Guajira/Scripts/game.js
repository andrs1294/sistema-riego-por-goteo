// create the Scene object

/**
var scene = sjs.Scene({w:640, h:480});

// load the images in parallel. When all the images are
// ready, the callback function is called.
scene.loadImages(['character.png'], function() {

    // create the Sprite object;
    var sp = scene.Sprite('character.png');

    // change the visible size of the sprite
    sp.size(55, 30);

    // apply the latest visual changes to the sprite
    // (draw if canvas, update attribute if DOM);
    sp.update();

    // change the offset of the image in the sprite
    // (this works the opposite way of a CSS background)
    sp.offset(50, 50);

    // various transformations
    sp.move(100, 100);
    sp.rotate(3.14 / 4);
    sp.scale(2);
    sp.setOpacity(0.8);

    sp.update();
});

**/


var canvas = document.getElementById("myCanvas2");
var listGround ;
var listPlant ;
var listTree ;
var listReservoir ;
var listCloud;
var listScareCrows;
var listCow ;
var xIniWorld=600;
var yIniWorld=270;

var canvasWidth = 1062;
var canvasHeight =  532;

var posCielo=170;

var cantidadTerrenoX = 8;
var cantidadTerrenoY = 7;
var cantidadTanque=3;

//Imagenes para bombeo 
var bombeo =0;

//Imagen actual del terreno
var imagGround = 0;

//Par de iamgen actual de las plantas
var imagPlant = 0;

//
var tipoCultivo = 1;

var weather ;
var numWeather=0;
var farm ;
var cloud ;
//Images for the Ground
var ground1Image = new Image();
ground1Image.src = "../Content/Imagenes/animacion/terreno/terreno1.png";


var ground2Image = new Image();
ground2Image.src = "../Content/Imagenes/animacion/terreno/terreno2.png";

var ground3Image = new Image();
ground3Image.src = "../Content/Imagenes/animacion/terreno/terreno3.png";

//Images for the Plants
var plant1Image = new Image();
plant1Image.src = "../Content/Imagenes/animacion/plantas/planta1.png";

var plant2Image = new Image();
plant2Image.src = "../Content/Imagenes/animacion/plantas/planta2.0.png";

var plant3Image = new Image();
plant3Image.src = "../Content/Imagenes/animacion/plantas/planta2.1.png";

var plant4Image = new Image();
plant4Image.src = "../Content/Imagenes/animacion/plantas/planta4.png";

var plant5Image = new Image();
plant5Image.src = "../Content/Imagenes/animacion/plantas/planta5.png";



//Images for the Weather
var moonImage = new Image();
moonImage.src = "../Content/Imagenes/animacion/clima/luna.png";

var cloudyImage = new Image();
cloudyImage.src = "../Content/Imagenes/animacion/clima/nublado.png";

var sunnyImage = new Image();
sunnyImage.src = "../Content/Imagenes/animacion/clima/sol.png";

var rainyImage = new Image();
rainyImage.src = "../Content/Imagenes/animacion/clima/llovizna.png";

//Image for Farm
var farmImage = new Image();
farmImage.src = "../Content/Imagenes/animacion/otros/decoracion3.png";

//Trees
var tree1Image = new Image();
tree1Image.src = "../Content/Imagenes/animacion/otros/decoracion6.png";

var tree2Image = new Image();
tree2Image.src = "../Content/Imagenes/animacion/otros/decoracion7.png";

//Cows
var cowImage = new Image();
cowImage.src = "../Content/Imagenes/animacion/otros/vaca.png";

//Cloud
var cloudImage = new Image();
cloudImage.src = "../Content/Imagenes/animacion/otros/nube.png";

//Decorations
var scareCrowImage = new Image();
scareCrowImage.src = "../Content/Imagenes/animacion/otros/decoracion1.png";

//Grass
var grassImage = new Image();
grassImage.src = "../Content/Imagenes/animacion/otros/grass.png";

//Grass
var skyImage = new Image();
skyImage.src = "../Content/Imagenes/animacion/otros/cielo.jpg";

//Reservoir
var reservoirImage = new Image();
reservoirImage.src = "../Content/Imagenes/animacion/otros/tanque1.png";

var reservoirImage2 = new Image();
reservoirImage2.src = "../Content/Imagenes/animacion/otros/hydrant.png";


function sprite (options) {

    var that = {};

    that.context = options.context;
    that.width = options.width;
    that.height = options.height;
    that.image = options.image;
    that.complementImage = options.complementImage;
    that.posX = parseInt(options.posX);
    that.posY = parseInt(options.posY);
    that.tamX = parseInt(options.tamX);
    that.tamY = parseInt(options.tamY);
    that.ImageChange=0;
    that.originalImage=options.image;


    that.render = function () {

        // Draw the animation
        that.context.drawImage(
           that.image,
           that.posX,
           that.posY,
           that.tamX,
           that.tamY);
    };


    that.update = function () {

        if(that.ImageChange==0)
        {
            that.image = that.complementImage;
            that.ImageChange=1;

        }else if(that.ImageChange==1)
        {
            that.image = that.originalImage;
            that.ImageChange=0;
        }
    }; 

    that.updatePos = function () {
        that.posX+=1;
        if(that.posX-200>canvasWidth)
        {
            that.posX=-50;
        }
    };

    return that;
}

function createGround(groundImage)
{
    listGround = new Array();

    var xIni=xIniWorld;
    var yIni=yIniWorld;
    var increaseX=34;
    var decreaseX2=increaseX*(cantidadTerrenoY+1);
    var increaseY1=15;
    var decreaseY2=increaseY1*(cantidadTerrenoY-1);

    for(var i=0;i<cantidadTerrenoX;i++)
    {
        for(var j=0;j<cantidadTerrenoY;j++)
        {

           var ground = sprite({
            context: canvas.getContext("2d"),
            width: 159,
            height: 138,
            image: groundImage,
            posX:xIni,
            posY:yIni,
            tamX:82,
            tamY:55
        });

           listGround.push(ground);

           if(j==parseInt(cantidadTerrenoY/(1+cantidadTanque)))
           {
            createReservoir(xIni-20,yIni-20);
        }
        xIni+=increaseX  ;
        yIni+=increaseY1;

    }
    xIni-=decreaseX2;
    yIni-=decreaseY2;
}
}


function createTrees()
{
    listTree = new Array();
    var xIni;
    var yIni;

    xIni=xIniWorld-25;
    yIni=yIniWorld-70;
    
    
    var increaseX=34;
    var decreaseX2=increaseX*(cantidadTerrenoY+1);
    var increaseY1=15;
    var decreaseY2=increaseY1*(cantidadTerrenoY-1);

    for(var i=0;i<cantidadTerrenoX;i++)
    {
        var tree = sprite({
            context: canvas.getContext("2d"),
            width: 159,
            height: 138,
            image: tree1Image,
            posX:xIni,
            posY:yIni,
            tamX:50,
            tamY:90
        });

        listTree.push(tree);
        for(var j=0;j<cantidadTerrenoY;j++)
        {

            if(j==cantidadTerrenoY-1)
            {
                var tree = sprite({
                    context: canvas.getContext("2d"),
                    width: 159,
                    height: 138,
                    image: tree1Image,
                    posX:xIni+70,
                    posY:yIni+60,
                    tamX:50,
                    tamY:90
                });
                listTree.push(tree);
            }
            xIni+=increaseX  ;
            yIni+=increaseY1;

        }
        xIni-=decreaseX2;
        yIni-=decreaseY2;
    }

    xIni=150;
    yIni=posCielo+70;
    tamaX=50;
    tamaY=85;
    tree1 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: tree2Image,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });
    xIni+=80;
    yIni+=50;
    tree2 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: tree2Image,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });
    xIni-=120;
    yIni-=110;
    tree3 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: tree2Image,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni-=80;
    yIni+=50;
    tree4 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: tree2Image,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

   // xIni-=80;
   yIni+=150;
   tree5 = sprite({
    context: canvas.getContext("2d"),
    width: 128,
    height: 128,
    image: tree2Image,
    posX:xIni,
    posY:yIni,
    tamX:tamaX,
    tamY:tamaY
});

   listTree.push(tree1);
   listTree.push(tree2);
   listTree.push(tree3);
   listTree.push(tree4);
   listTree.push(tree5);
}







function drawObject(listObj)
{
    for(var i=0;i<listObj.length;i++)
    {
        listObj[i].render();
    }
}

function createPlant(plantImage,complementImagen)
{
    listPlant = new Array();

    var xIni=xIniWorld+15;
    var yIni=yIniWorld-18;
    var increaseX=34;
    var decreaseX2=increaseX*(cantidadTerrenoY+1);
    var increaseY1=15;
    var decreaseY2=increaseY1*(cantidadTerrenoY-1);

    for(var i=0;i<cantidadTerrenoX;i++)
    {
        for(var j=0;j<cantidadTerrenoY;j++)
        {

           var plant = sprite({
            context: canvas.getContext("2d"),
            width: 159,
            height: 138,
            image: plantImage,
            complementImage: complementImagen,
            posX:xIni,
            posY:yIni,
            tamX:45,
            tamY:40
        });

           listPlant.push(plant);

           xIni+=increaseX  ;
           yIni+=increaseY1;

       }
       xIni-=decreaseX2;
       yIni-=decreaseY2;
   }
}

function createReservoir(xIni,yIni){

    listReservoir = new Array();
    xIni-=70;
    yIni+=30,
    imagen = reservoirImage;
    if(bombeo)
    {
        imagen=reservoirImage2;
        //alert('Imagen Bombeo no establecida');
    }
    for(var i=0;i<cantidadTanque;i++)
    {
        var reservoir1 = sprite({
            context: canvas.getContext("2d"),
            width: 159,
            height: 138,
            image: imagen,
            posX:xIni,
            posY:yIni,
            tamX:60,
            tamY:80
        });
        xIni+=40;
        yIni+=20;
        listReservoir.push(reservoir1);
    }
}


function createWeather(weatherImage)
{

    var xIni=35;
    var yIni=25;


    weather = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: weatherImage,
        posX:xIni,
        posY:yIni,
        tamX:90,
        tamY:86
    });



   // weather.render();


}



function gameUpdateImages(listObj) {

    for(var i=0;i<listObj.length;i++)
    {
        listObj[i].update();
       // listObj[i].render();
   }

}

function updatePosCloud()
{
    for(var i=0;i<listCloud.length;i++)
    {
        listCloud[i].updatePos();
        listCloud[i].render();
    }
}

function drawAll()
{
    canvas.getContext("2d").clearRect(0, posCielo, canvas.width, canvas.height);
    drawGrass();
    drawObject(listGround);
    drawObject(listTree);
    drawObject(listCow);
    drawObject(listCloud);

    if(listPlant != null)
    {
        drawObject(listPlant);
    }
    
    farm.render();
    drawObject(listScareCrows);
    drawObject(listReservoir);
}

function createFarm()
{
    var xIni=xIniWorld+(cantidadTerrenoY/2)*34+50;
    var yIni=yIniWorld-100;


    farm = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: farmImage,
        posX:xIni,
        posY:yIni,
        tamX:190,
        tamY:145
    });
}

function createScareCrow()
{
    var xIni=farm.posX-100;
    var yIni=farm.posY;
    listScareCrows = new Array();
    var tamaX=140;
    var tamaY=125;

    var scareCrow1 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: scareCrowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });
    xIni+=130;
    yIni+=80;
    var scareCrow2 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: scareCrowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    listScareCrows.push(scareCrow1);
    listScareCrows.push(scareCrow2);
}

function createCows()
{
    var xIni=xIniWorld+(cantidadTerrenoY/2)*34+220;
    var yIni=yIniWorld;
    var tamaX=75;
    var tamaY=70;
    listCow = new Array();
    cow1 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni+=40;
    yIni+=100;
    cow2 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni-=80;
    yIni-=45;
    cow3 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    listCow.push(cow1);
    listCow.push(cow2);
    listCow.push(cow3);

}


function createClouds()
{
    var xIni=xIniWorld+(cantidadTerrenoY/2)*34+220;
    var yIni=33;
    var tamaX=120;
    var tamaY=70;
    listCloud = new Array();
    

    cloud1 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cloudImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni-=400;
    yIni+=25;
    cloud2 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cloudImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni-=140;
    yIni-=45;
    cloud3 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cloudImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    listCloud.push(cloud1);
    listCloud.push(cloud2);
    listCloud.push(cloud3);

}

function drawGrass()
{
    var tamaXgrass=20;
    var tamaYgrass=40;
    posxIni=0;
    posyIni=posCielo-15;
    var posXincrease=tamaXgrass;
    var posYincrease=tamaYgrass-18;
    var ctx = canvas.getContext("2d");
    for(var i=0;i<(canvasWidth/tamaXgrass);i++)
    {
        for(var j=0;j<(canvasHeight-posCielo/tamaYgrass);j++)
        {
            ctx.drawImage( grassImage,2,0,98,130, posxIni, posyIni, tamaXgrass, tamaYgrass);
            posxIni+=posXincrease;
        }
        posxIni=0;
        posyIni+=posYincrease;
    }
}

function drawSky()
{
   // var tamaXgrass=20;
   var tamaYgrass=40;
   posxIni=0;
   posyIni=0;
  //  var posXincrease=tamaXgrass;
  //  var posYincrease=tamaYgrass-18;
  var ctx = canvas.getContext("2d");
  //  for(var i=0;i<(canvasWidth/tamaXgrass);i++)
  //  {
  //      for(var j=0;j<(canvasHeight-posCielo/tamaYgrass);j++)
  //      {
    ctx.drawImage( skyImage, posxIni, posyIni, canvasWidth, posCielo);
  //          posxIni+=posXincrease;
  //      }
  //          posxIni=0;
  //         posyIni+=posYincrease;
  //  }
}


function createAndDrawTomato(groundImage,plantImage,complementplantImage,weatherImage)
{

    createGround(groundImage);
    createPlant(plantImage,complementplantImage);
    createFarm();
    createCows();
    createTrees();
    createClouds();
    createWeather(weatherImage);
    createScareCrow();

    drawAll();
    setInterval(function() { updateGround(); },1999);
    setInterval(function() { updatePlants(); },10000);
   // setInterval(function() { drawAll(); },2000);
   setInterval(function() { updateClouds(); },80);

}

function drawPotato(groundImage,weatherImage)
{
    createGround(groundImage);
    createFarm();
    createCows();
    createTrees();
    createClouds();
    createWeather(weatherImage);
    createScareCrow();
    //drawObject(listGround);
    drawAll();

    setInterval(function() { updateGround(); },10000);

    //setInterval(function() { drawAll(); },2000);
    setInterval(function() { updateClouds(); },70);
}

function updatePlants()
{

    for(var i = 0;i<listPlant.length;i++)
    {
        if(imagPlant==0)
        {
            listPlant[i].originalImage = plant1Image;
            listPlant[i].complementImage = plant2Image;

        }else if(imagPlant == 1)
        {
         listPlant[i].originalImage = plant2Image;
         listPlant[i].complementImage = plant3Image;

     }else if(imagPlant == 2)
     {
         listPlant[i].originalImage = plant3Image;
         listPlant[i].complementImage = plant4Image;
     }
     else if(imagPlant == 3)
     {
         listPlant[i].originalImage = plant4Image;
         listPlant[i].complementImage = plant5Image;
     }

       // drawAll();
       // setTimeout(function() { }, 1000);

   }
   imagPlant++;
   if(imagPlant>=4)
   {
    imagPlant=3;
}
}

function updateGround()
{
    if(tipoCultivo==0)
    {
        for(var i = 0;i<listGround.length;i++)
        {
            if(imagGround==0)
            {
                listGround[i].image = ground1Image;

            }else if(imagGround == 1)
            {
                listGround[i].image = ground2Image;

            }else if(imagGround == 2)
            {
                listGround[i].image = ground3Image;

            }

       // drawAll();
       // setTimeout(function() { }, 1000);

   }
   imagGround++;
   if(imagGround>=3)
   {
    imagGround=0;
}

}else if(tipoCultivo==1)
{
    gameUpdateImages(listPlant);
}

drawAll();

}


function updateClouds()
{
    //canvas.getContext("2d").clearRect(0, 0, canvas.width, posCielo);
    drawSky();
    updatePosCloud();
    weather.render();
}

$( document ).ready(function() {
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    
    getFechas();
    tipoCultivo = getTipoCultivo();
    cantidadTerrenoY = getTamCultivo(1);
    cantidadTerrenoX = getTamCultivo(0);
    var tipoTanque = getTipoTanque();
    if(tipoTanque === "false")
    {
        bombeo=0;
    }else{
        bombeo=1;
    }
    
    cantidadTanque = getCantidadTanques();
    //alert(tipoCultivo+" "+cantidadTerrenoY+" "+cantidadTerrenoX+" "+cantidadTanque);
    animarTodo(tipoCultivo);
    //createAndDrawTomato(ground1Image,plant2Image,plant3Image,moonImage);
});

function getFechas()
{
    getFechaSiembra();
    getFechaCosecha();
}

function getFechaSiembra()
{
    fecha=""
    $.ajax({
        url: '/Panel/FechaSiembra',
        type: "POST",
        async: false,
        success: function (html) {
            fecha = html;
        //alert(fecha);

    },
    error: function () {
        alert('error');
    }

});
    aux = fecha.split("/");
    day = aux[0];
    month = aux[1];
    aux2 = aux[2].split(" ");
    year = aux2[0];

    $('#month_yearS').html(numberToName(month) );
    $('#dayS').html(day);
}

function getFechaCosecha()
{
 fecha = "";
 $.ajax({
    url: '/Panel/fechaCosecha',
    type: "POST",
    async: false,
    success: function (html) {
        fecha = html;
        
    },
    error: function () {
        alert('error');
    }

});
 if(fecha != null && fecha !== "" )
 {
   aux = fecha.split("/");
   day = aux[0];
   month = aux[1];
   aux2 = aux[2].split(" ");
   year = aux2[0];

   $('#month_yearC').html(numberToName(month) );
   $('#dayC').html(day);
}


}

function numberToName(num)
{
    if(num==1)
    {
        return "Ene";
    }
    if(num==2)
    {
        return "Feb";
    }
    if(num==3)
    {
        return "Mar";
    }
    if(num==4)
    {
        return "Abr";
    }
    if(num==5)
    {
        return "May";
    }
    if(num==6)
    {
        return "Jun";
    }
    if(num==7)
    {
        return "Jul";
    }
    if(num==8)
    {
        return "Ago";
    }
    if(num==9)
    {
        return "Sep";
    }
    if(num==10)
    {
        return "Oct";
    }
    if(num==11)
    {
        return "Nov";
    }
    if(num==12)
    {
        return "Dic";
    }
}


function getTipoCultivo()
{
    var tipo = 0;
    $.ajax({
        url: '/Panel/TipoCultivo',
        type: "POST",
        async: false,
        success: function (html) {
            if(html == 'Papa')
            {
                tipo = 0;
            }else if(html == 'Tomate')
            {
                tipo = 1;
            }

            

        },
        error: function () {
            alert('error');
        }

    });
    return tipo;
}

function getTipoTanque()
{
    tipoTanque = "";
    //Llamado al ajax
    $.ajax({
        url: '/Panel/TipoTanque',
        type: "POST",
        async: false,
        success: function (html) {

            tipoTanque = html;
        },
        error: function () {
            alert('error');
        }

    });
    return tipoTanque;
}

function animarTodo( tipo)
{
    //0 papa
    if(tipo==0)
    {
        drawPotato(ground1Image,rainyImage);

    }else if(tipo==1)
    {
        createAndDrawTomato(ground1Image,plant1Image,plant2Image,sunnyImage);
    }
     setInterval(function() { updateWeather(); },10000);
}
function updateWeather()
{
    if(numWeather == 0)
    {
        weather.image = sunnyImage;
        numWeather++;
    }else if(numWeather == 1)
    {
         weather.image = cloudyImage;
        numWeather++;
    }else if(numWeather == 2)
    {
         weather.image = rainyImage;
        numWeather++;
    }else if(numWeather == 3)
    {
        weather.image = moonImage;
        numWeather=0;
    }
    
}

function getTamCultivo( num)
{
    //var texto = getTamCultivo2();

    var texto ="";

    $.ajax({
        url: '/Panel/TamanoCultivo',
        type: "POST",
        async: false,
        success: function (html) {
            texto= html;
        },
        error: function () {
            alert('error');
        }

    });


   // alert(texto);
   var arrays = texto.split(",");
   if(num == 0)
   {   
    var anch = parseInt(arrays[0]);
    var anch = anch/100;
    if(anch>9)
        {return 9;}
    if(anch<=0)
        {return 1;}
    return anch;
}else if(num == 1)
{
    var alt = parseInt(arrays[1]);
    var alt = alt/100;
    if(alt>9)
        {return 9;}
    if(alt<=0)
        {return 1;}
    return alt;
}
return 1;
}


function getCantidadTanques()
{
    var cant = 0;
    //Llamado al ajax
    $.ajax({
        url: '/Panel/CantidadTanque',
        type: "POST",
        async: false,
        success: function (html) {
            cant = parseInt(html);

            if(cant>=4)
            {
                cant = 4;
            }
            if(cant<=1)
            {
                cant = 1;
            }
        //cantidadTanque = cant;

    },
    error: function () {
        alert('error');
    }

});
    return cant;
}