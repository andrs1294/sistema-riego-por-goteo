﻿using DTO.Rancheria;
using Guajira.Seguridad;
using Guajira.Utilidades;
using MODELO.Interfaces.Rancheria;
using MODELO.Modelos.Rancheria;
using System;
using System.Web.Mvc;

namespace Guajira.Controllers
{
    [Autorizacion]
    public class CultivoController : Controller
    {
        ICultivo modelo = new ModCultivo();
        ITanque modeloTanque = new ModTanque();
        IPlanta modeloPlanta = new ModPlanta();
        ILineaSuministro modeloSuministro = new ModLineaSuministro();





        [HttpPost]
        public void recibirIdCultivo()
        {
            string id = Request.Form["idCultivo"];
            // idCultivo = int.Parse(id);
            Session["Idc"] = id;

        }

        //[HttpPost]
        //public void BorrarCultivo()
        //{
        //    recibirIdCultivo();
        //    ClsCultivo r = new ClsCultivo();
        //    r.id = (int)Session["Idc"];

        //    modelo.BorrarCultivo(r);

        //}

        [HttpPost]
        public String Crear()
        {
            ClsCultivo model = new ClsCultivo();
            model.alto= int.Parse(Request.Form["alto"]);
            model.ancho= int.Parse(Request.Form["ancho"]);
            model.idRaancheria = (int)Session["Idr"];
            model.fechaSiembra = DateTime.ParseExact(Request.Form["fecha"],"yyyy-MM-dd",System.Globalization.CultureInfo.InvariantCulture);
            //model.fechaSiembra = System.DateTime.ParseExact(Request.Form["fecha"]);
            int resul = modelo.Crear(model);


            if (resul>=0)
            {
                Session["Idc"] = resul;
                
                ViewBag.Mensaje = Alertas.EXITO(" Cultivo ingresado con exito! ");
            }
            else
            {
                ViewBag.Mensaje = Alertas.EXITO(" Error creando cultivo! ");
            }

            return resul+"" ;
        }

        public ActionResult Listar()
        {
            return View(modelo.ListarCultivo());
        }

        [HttpPost]
        public string IdR()
        {
            int a = (int)Session["Idr"];

            string html = "<input class='form-control' readonly id='CargarR' name='CargarR' value='" + a + "' />";
            return html;
        }


       

        [HttpPost]
        public String CrearTanque()
        {
            ClsTanque model = new ClsTanque();
            model.cantidad_tanques = int.Parse(Request.Form["ctanques"]);
            //guado cuantos tanques se solicitaron
            Session["ctanques"] = model.cantidad_tanques;
            model.capacidad= int.Parse(Request.Form["capacidad"]);
            model.bombeo= Request.Form["bombeo"];
            model.id_rancheria = (int)Session["Idr"];
            model.id_cultivo = (int)Session["Idc"];
            bool resul = modeloTanque.Crear(model);
            if (resul)
            {
                ViewBag.Mensaje = Alertas.EXITO(" Tanque creado con exito! ");
            }
            else
            {
                ViewBag.Mensaje = Alertas.EXITO(" Error creando el tanque! ");
            }

            return resul + "";
        }
        [HttpPost]
        public String CrearSuministro()
        {
            ClsLineaSuministro model = new ClsLineaSuministro();

            model.lineaObstruida = "false";

            model.litrosPorSegundo = int.Parse(Request.Form["litrosps"]);
            model.cultivo = (int)Session["Idc"];
            
            bool resul = modeloSuministro.Crear(model);
            if (resul)
            {
                ViewBag.Mensaje = Alertas.EXITO(" Tanque creado con exito! ");
            }
            else
            {
                ViewBag.Mensaje = Alertas.EXITO(" Error creando el tanque! ");
            }

            return resul + "";
        }

        //creamos tantas plantas sea posible, para esto se necesita el tamaño del cultivo


        [HttpPost]
        public String CrearPlanta()
        {
            int id_planta = int.Parse(Request.Form["idplanta"]);
            ClsPlanta r = new ClsPlanta();
            if (id_planta == 0)
            {
                r.nombre = "Papa";
                r.nutrientes = 2;
                r.tamano = 1;
            }
            else if (id_planta == 1)
            {
                r.nombre = "Tomate";
                r.nutrientes = 1;
                r.tamano = 1;
            }
            r.cultivo = (int)Session["Idc"];
            
            bool resul = modeloPlanta.Crear(r);
            if (resul)
            {
                ViewBag.Mensaje = Alertas.EXITO("Planta creada Satisfactoriamente");
            }
            else
            {
                ViewBag.Mensaje = Alertas.ERROR("No se pudo crear la planta,intentelo despues");

            }

            return resul + "";
        }

        public ActionResult Panel()
        {
            return View();
        }

        public ActionResult grafica1()
        {
            return PartialView();
        }
        public ActionResult grafica2()
        {
            return PartialView();
        }

        public ActionResult grafica3()
        {
            return PartialView();
        }


    }
}
