﻿using DTO.Seguridad;
using MODELO.Interfaces;
using MODELO.Modelos;
using Guajira.Seguridad;
using Guajira.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Guajira.Controllers
{
    public class UsuarioController : Controller
    {
 
        [AllowAnonymous]
        public ActionResult IniciarSesion()
        {
            return View(new ClsUsuario());
        }

        [HttpPost]
        public ActionResult IniciarSesion(ClsUsuario model)
        {
            IUsuario modelo = new ModUsuario();
            ClsUsuario dto = modelo.IniciarSesion(model);
            if (dto != null)
            {
                Sesion.Usuario = dto.usuario;
                Sesion.Id = dto.id.ToString();
                Sesion.Roles = modelo.ListarRoles(dto);
                Sesion.Menu = modelo.ListarMenus(dto);

                return RedirectToAction("Listar", "Rancheria");
            }
            else
            {
                ViewBag.Mensaje = Alertas.ERROR("Usuario y contraseña incorrectos.");
                return View();
            }
        }
        public ActionResult CerrarSesion()
        {
            Sesion.Usuario = null;
 

                return RedirectToAction("IniciarSesion", "Usuario");
            
  
        }
    }
}
