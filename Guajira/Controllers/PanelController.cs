﻿using Guajira.Seguridad;
using MODELO.Interfaces.Rancheria;
using MODELO.Modelos.Rancheria;
using System.Web.Mvc;

namespace Guajira.Controllers
{
    [Autorizacion]

    public class PanelController : Controller
    {
        ICultivo modeloCultivo = new ModCultivo();
        ITanque modeloTanque = new ModTanque();
        // GET: Panel
        //int idCultivo = -1;

    

        [HttpPost]
        public string TipoCultivo()
        {
            string idc = Session["Idc"].ToString();

            int id = int.Parse(idc);
            string resu= modeloCultivo.GetTipo(id);
            return resu;
        }

        //ancho, alto
        [HttpPost]
        public string TamanoCultivo()
        {
            string idc = Session["Idc"].ToString();

            int id = int.Parse(idc);
            string resu= modeloCultivo.GetTamano(id); 
            return resu;
        }
        [HttpPost]
        public string TipoTanque()
        {

            string idc = Session["Idc"].ToString();

            int id = int.Parse(idc);
            string resu= modeloTanque.GetTipo(id); 
            return resu;
        }
        [HttpPost]
        public string CantidadTanque()
        {

            string idc = Session["Idc"].ToString();

            int id = int.Parse(idc);
             
            string resu= modeloTanque.GetCantidad(id);
            return resu;
        }

        [HttpPost]
        public string FechaSiembra()
        {

            string idc = Session["Idc"].ToString();

            int id = int.Parse(idc);

            string resu = modeloCultivo.GetSiembra(id);
            return resu;
        }
        [HttpPost]
        public string FechaCosecha()
        {

            string idc = Session["Idc"].ToString();

            int id = int.Parse(idc);

            string resu = modeloCultivo.GetCosecha(id);
            return resu;
        }
    }
}