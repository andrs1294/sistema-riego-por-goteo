﻿using DTO.Rancheria;
using Guajira.Seguridad;
using Guajira.Utilidades;
using MODELO.Interfaces.Rancheria;
using MODELO.Modelos.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Guajira.Controllers
{
    [Autorizacion]
    public class RegistroClimaController : Controller
    {
       
            IRegistroClima modelo = new ModRegistroClima();
            [HttpGet]
            public ActionResult Crear()
            {

                return View(new ClsRegistroClima());
            }
            [HttpPost]
            public ActionResult Crear(ClsRegistroClima model)
            {
              
                if (modelo.Crear(model))
                {
                    ViewBag.Mensaje = Alertas.EXITO("Registro del clima fue creado Satisfactoriamente");
                }
                else
                {
                    ViewBag.Mensaje = Alertas.ERROR("No se pudo crear el registor del clima,intentelo despues");

                }

                return View();
            }

            public ActionResult Listar()
            {

                List<ClsRegistroClima> climas2 = modelo.ListarClima();
                List<ClsRegistroClima> climas = new List<ClsRegistroClima>();

                if (climas2 == null)
                {
                    return View(climas);
                }
                else
                {
                    for (int i = 0; i < climas2.Count; i++)
                    {
                        if (climas2[i].usuario.ToString().Equals(Sesion.Id))
                        {
                            climas.Add(climas2[i]);
                        }
                    }

                    if (Sesion.Id.Equals("1"))
                    {
                        return View(climas2);
                    }
                    else
                    {
                        return View(climas);
                    }

                }

            }

            [HttpGet]
            public ActionResult Borrar(int id)
            {
                ClsRegistroClima r = new ClsRegistroClima();
                r.id = id;

                if (modelo.Borrar(r))
                {
                    ViewBag.Mensaje = Alertas.EXITO("Registro del clima Eliminado Satisfactoriamente");
                }
                else
                {
                    ViewBag.Mensaje = Alertas.ERROR("No se pudo Eliminar el registro del clima,intentelo despues");

                }

                return RedirectToAction("Listar");

            }
        }
}