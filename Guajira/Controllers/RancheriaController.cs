﻿
using DTO.Rancheria;
using Guajira.Seguridad;
using Guajira.Utilidades;
using MODELO.Interfaces.Rancheria;
using MODELO.Modelos.Rancheria;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Guajira.Controllers
{
    [Autorizacion]

    public class RancheriaController : Controller
    {
        IRancheria modelo = new ModRancheria();
        ICultivo modcult = new ModCultivo();

        [HttpGet]
        public ActionResult Crear()
        {

            return View(new ClsRancheria());
        }
        [HttpPost]
        public ActionResult Crear(ClsRancheria model)
        {
            ClsRancheria r = new ClsRancheria();
            r.nombre = model.nombre;
            r.latitud = model.latitud;
            r.longitud = model.longitud;
            r.usuario = int.Parse(Sesion.Id);
            if (modelo.Crear(r))
            {
                ViewBag.Mensaje = Alertas.EXITO("Rancheria creada Satisfactoriamente");
            }
            else
            {
                ViewBag.Mensaje = Alertas.ERROR("No se pudo crear la rancheria,intentelo despues");

            }

            return View();
        }

        public ActionResult Listar()
        {

            List<ClsRancheria> rancherias2 = modelo.ListarRancheria();
            List<ClsRancheria> rancherias = new List<ClsRancheria>();

            if (rancherias2 == null)
            {
                return View(rancherias);
            }
            else
            {
                for (int i = 0; i < rancherias2.Count; i++)
                {
                    if (rancherias2[i].usuario.ToString().Equals(Sesion.Id))
                    {
                        rancherias.Add(rancherias2[i]);
                    }
                }

                if (Sesion.Id.Equals("1"))
                {
                    return View(rancherias2);
                }
                else
                {
                    return View(rancherias);
                }

            }

        }

        [HttpGet]
        public ActionResult Borrar(int id)
        {
            ClsRancheria r = new ClsRancheria();
            r.id = id;

            if (modelo.Borrar(r))
            {
                ViewBag.Mensaje = Alertas.EXITO("Rancheria Eliminada Satisfactoriamente");
            }
            else
            {
                ViewBag.Mensaje = Alertas.ERROR("No se pudo Eliminar la rancheria,intentelo despues");

            }

            return RedirectToAction("Listar");

        }

        [HttpGet]
        public ActionResult BorrarCultivo(int id)
        {
            ClsCultivo r = new ClsCultivo();
            r.id = id;

            if (modcult.BorrarCultivo(r))
            {
                ViewBag.Mensaje = Alertas.EXITO("Cultivo Eliminada Satisfactoriamente");
            }
            else
            {
                ViewBag.Mensaje = Alertas.ERROR("No se pudo Eliminar el cultivo,intentelo despues");

            }

            return RedirectToAction("Ir/"+Session["idR"]);

        }




        [HttpPost]
        public string obtenerLocalizaciones()
        {
            List<ClsRancheria> rancherias2 = modelo.ListarRancheria();
            List<ClsRancheria> rancherias = new List<ClsRancheria>();

            if (rancherias2 == null)
            {
                return "no hay datos";
            }
            else
            {


                for (int i = 0; i < rancherias2.Count; i++)
                {
                    if (rancherias2[i].usuario.ToString().Equals(Sesion.Id))
                    {
                        rancherias.Add(rancherias2[i]);
                    }
                }
                string html = "";
                if (Sesion.Id.Equals("1"))
                {

                    for (int i = 0; i < rancherias2.Count; i++)
                    {

                        html += rancherias2[i].nombre + "," + rancherias2[i].latitud + "," + rancherias2[i].longitud + ",";


                    }
                }
                else
                {
                    for (int i = 0; i < rancherias.Count; i++)
                    {

                        html += rancherias[i].nombre + "," + rancherias[i].latitud + "," + rancherias[i].longitud + ",";


                    }
                }

                Console.WriteLine(html);
                return html;
            }


        }

        public ActionResult Ir(int id)
        {

            Session["idR"] = id;
            ICultivo mod = new ModCultivo();

            List<ClsCultivo> cult = new List<ClsCultivo>();

            List<ClsCultivo> cult2 = mod.ListarCultivo();
            if (cult2 != null)
            {
                for (int i = 0; i < cult2.Count; i++)
                {
                    if (cult2[i].idRaancheria == id)
                    {
                        cult.Add(cult2[i]);
                    }
                }
            }

            return View(cult);
        }

    }
}
