﻿using DTO.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Guajira.Seguridad
{
    public class Principal : IPrincipal
    {
        private ClsUsuario Usuario;

        public Principal(ClsUsuario usuario)
        {
            Usuario = usuario;
            Identity = new GenericIdentity(Usuario.usuario);
        }


        public IIdentity Identity
        {
            get;
            set;
        }

        public bool IsInRole(string role)
        {
            var roles = role.Split(new char[] { ',' });
            return roles.Any(r => Usuario.roles.Contains(r));
        }
    }
}