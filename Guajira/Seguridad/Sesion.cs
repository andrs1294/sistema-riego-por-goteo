﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Guajira.Seguridad
{
    public static class Sesion
    {
        static string usernameSesionvar = "usuario";
        public static string Usuario
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return string.Empty;
                }
                var sessionVar = HttpContext.Current.Session[usernameSesionvar];
                if (sessionVar != null)
                {
                    return sessionVar as string;
                }

                return null;
            }

            set
            {
                HttpContext.Current.Session[usernameSesionvar] = value;
            }
        }

        static string idSesionvar = "id";
        public static string Id
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return string.Empty;
                }
                var sessionVar = HttpContext.Current.Session[idSesionvar];
                if (sessionVar != null)
                {
                    return sessionVar as string;
                }

                return null;
            }

            set
            {
                HttpContext.Current.Session[idSesionvar] = value;
            }
        }

        static string menuSesionvar = "Menu";
        public static string Menu
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return string.Empty;
                }
                var sessionVar = HttpContext.Current.Session[menuSesionvar];
                if (sessionVar != null)
                {
                    return sessionVar as string;
                }

                return null;
            }

            set
            {
                HttpContext.Current.Session[menuSesionvar] = value;
            }
        }

        static string rolesSesionvar = "Roles";
        public static List<string> Roles
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return null;
                }
                var sessionVar = HttpContext.Current.Session[rolesSesionvar];
                if (sessionVar != null)
                {
                    return sessionVar as List<string>;
                }

                return null;
            }

            set
            {
                HttpContext.Current.Session[rolesSesionvar] = value;
            }
        }
    }
}