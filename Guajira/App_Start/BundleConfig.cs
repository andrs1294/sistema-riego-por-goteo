﻿using System.Web;
using System.Web.Optimization;

namespace Guajira
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/estilosIndex").Include(
                      "~/Content/assets/plugins/custombox/css/custombox.min.css",
                      "~/Content/assets/plugins/bootstrap-sweetalert/sweet-alert.css",
                      "~/Content/assets/plugins/morris/morris.css",
                      "~/Content/assets/plugins/switchery/switchery.min.css",
                      "~/Content/assets/css/style.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/modernizrIndex").Include(
                        "~/Content/assets/js/modernizr.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/ScriptsIndex").Include(
                        "~/Content/assets/js/jquery.min.js",
                        "~/Content/assets/js/tether.min.js",
                        "~/Content/assets/js/bootstrap.min.js",
                        "~/Content/assets/js/detect.js",
                        "~/Content/assets/js/fastclick.js",
                        "~/Content/assets/js/jquery.blockUI.js",
                        "~/Content/assets/js/waves.js",
                        "~/Content/assets/js/jquery.nicescroll.js",
                        "~/Content/assets/js/jquery.scrollTo.min.js",
                        "~/Content/assets/js/jquery.slimscroll.js",
                        "~/Content/assets/plugins/switchery/switchery.min.js",
                        "~/Content/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js",
                        "~/Content/assets/pages/jquery.sweet-alert.init.js",
                        "~/Content/assets/plugins/morris/morris.min.js",
                        "~/Content/assets/plugins/raphael/raphael-min.js",
                        "~/Content/assets/plugins/waypoints/lib/jquery.waypoints.js",
                        "~/Content/assets/plugins/counterup/jquery.counterup.min.js",
                        "~/Content/assets/plugins/custombox/js/custombox.min.js",
                        "~/Content/assets/plugins/custombox/js/legacy.min.js",
                        "~/Content/assets/js/jquery.core.js",
                        "~/Content/assets/js/jquery.app.js",
                        "~/Content/assets/pages/jquery.dashboard.js"

                        ));
        }
    }
}
