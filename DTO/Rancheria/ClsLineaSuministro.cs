﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
    public class ClsLineaSuministro
    {
        [Key]
        public int id { set; get; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Litros por segundo")]
        public int litrosPorSegundo { set; get; }


        [DataType(DataType.Text)]
        [Display(Name = "cultivo")]
        public int cultivo { set; get; }

        [Required]
        [Display(Name = "Tanque")]
        public int tanque { get; set; }

        [Required]
        [Display(Name = "Linea Obstruida")]
        public String lineaObstruida { get; set; }
    }
}
