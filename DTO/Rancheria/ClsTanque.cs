﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
   public class ClsTanque
    {
        [Key]
        public int id { set; get; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Capacidad")]

        public int capacidad { set; get; }
        [DataType(DataType.Text)]
        [Display(Name = "Estado")]

        public string estado { set; get; }
        [DataType(DataType.Text)]
        [Display(Name = "Bombeo")]

        public string bombeo { set; get; }
        [DataType(DataType.Text)]
        [Display(Name = "nivel Actual")]
        public int nivel_actual { set; get; }
        [DataType(DataType.Text)]
        [Display(Name = "Rancheria")]
        public int id_rancheria { set; get; }
        [DataType(DataType.Text)]
        [Display(Name = "Rancheria")]
        public int id_cultivo { set; get; }
        [DataType(DataType.Text)]
        [Display(Name = "Cantidad")]
        public int cantidad_tanques { set; get; }

    }
}
