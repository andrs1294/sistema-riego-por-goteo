﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
    public class ClsRegistroCrecimiento
    {
        [Key]
        public int id { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de registro crecimiento")]
        public DateTime fecha { set; get; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Valor")]

        public decimal valor { set; get; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Planta")]
        public int fk_planta { set; get; }
    }
}
