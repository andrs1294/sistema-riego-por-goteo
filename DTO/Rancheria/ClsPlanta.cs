﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
   public class ClsPlanta
    {
        [Key]
        public int id { set; get; }
        [Required]
        [Display(Name ="Nombre")]
        [DataType(DataType.Text)]

        public string nombre { set; get; }
        [Required]
        [Display(Name = "Nutrientes")]
        [DataType(DataType.Text)]


        public decimal nutrientes { set; get; }
        [Display(Name = "Tamaño")]
        [DataType(DataType.Text)]

        public int tamano { set; get; }
        [Required]
        [Display(Name = "Cultivo")]
        [DataType(DataType.Text)]


        public int cultivo { set; get; }
    }
}
