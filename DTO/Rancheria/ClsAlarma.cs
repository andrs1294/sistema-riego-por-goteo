﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Rancheria
{
    public class ClsAlarma
    {
        [Key]
        public int id { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha")]
        public DateTime fecha { get; set; }

        [Required]
        [Display(Name = "Mensaje")]
        public string mensaje { set; get; }

        [Required]
        [Display(Name = "Cultivo")]
        public int cultivo { get; set; }

      
    }
}
