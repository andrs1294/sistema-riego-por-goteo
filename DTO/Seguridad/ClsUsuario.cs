﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DTO.Seguridad
{
    public class ClsUsuario
    {
        [Key]
        public int id { get; set; }

        [Required]
        [Display(Name = "Usuario")]
        [DataType(DataType.Text)]
        public string usuario { get; set; }

        [Required]
        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        [StringLength(12, ErrorMessage = "La contraseña debe tener máximo 12 caracteres y minimo 6", MinimumLength = 6)]
        public string contrasena { get; set; }

        public List<string> roles { get; set; }
    }
}
