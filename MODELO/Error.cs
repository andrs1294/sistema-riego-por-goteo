﻿using MODELO.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO
{
    public static class Error
    {
        public static void INGRESAR(Exception e, string clase, string metodo)
        {
            bool bandera = true;
            while (bandera)
            {
                if (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                else
                {
                    using (ESeguridad db = new ESeguridad())
                    {
                        Seg_Error error = new Seg_Error();
                        error.ERROR = e.Message;
                        error.TRACE = e.StackTrace;
                        error.METODO = metodo;
                        error.CLASE = clase;

                        db.Seg_Error.Add(error);
                        db.SaveChanges();

                        bandera = false;
                    }
                }
            }
        }
    }
}

