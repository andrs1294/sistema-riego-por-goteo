﻿using DTO.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces
{
    public interface IUsuario
    {
        /// <summary>
        /// Valida usuario y contraseña del DTO y retorna todo el usuario
        /// </summary>
        /// <param name="dto">Usuario a iniciar sesion</param>
        /// <returns>Usuario si valida los datos, Null de lo contrario</returns>
        ClsUsuario IniciarSesion(ClsUsuario dto);


        /// <summary>
        /// Dado el nombre de usuario trae todos los datos de ese usuario
        /// </summary>
        /// <param name="usuario">Username con el nombre de usuario necestido</param>
        /// <returns>Un ClsUsuario si encuentra la coincidencia</returns>
        ClsUsuario ListarUsuario(string usuario);


        /// <summary>
        /// Lista los roles dado un usuario
        /// </summary>
        /// <param name="dto">ClsUsuario que contiene la informacion para validar</param>
        /// <returns>string con los roles concatenados por "," </returns>
        List<string> ListarRoles(ClsUsuario dto);


        /// <summary>
        /// Lista los menus a los cuales tiene acceso el usuario dado los roles
        /// </summary>
        /// <param name="dto">Usuario a traer roles</param>
        /// <returns>string con html con las opciones de menu</returns>
        string ListarMenus(ClsUsuario dto);
    }
}
