﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces.Rancheria
{
    public interface ITipoNutriente
    {
        bool Crear(ClsTipoNutriente dto);

        bool Borrar(ClsTipoNutriente dto);

        List<ClsAlarma> ListarTipoNutriente();
    }
}
