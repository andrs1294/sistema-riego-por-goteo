﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces.Rancheria
{
    public interface ICultivo
    {
        int Crear(ClsCultivo dto);

        bool BorrarCultivo(ClsCultivo dto);

        List<ClsCultivo> ListarCultivo();
        string GetTipo(int id);
        string GetTamano(int id);
        string GetSiembra(int id);
        string GetCosecha(int id);
    }
}
