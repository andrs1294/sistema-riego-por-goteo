﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;


namespace MODELO.Interfaces.Rancheria
{
    public interface IPlanta
    {
        bool Crear(ClsPlanta dto);

        bool Editar(ClsPlanta dto);
        int Calcular(int ancho, int alto, ClsPlanta dto);

    }
}
