﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces.Rancheria
{
    public interface ILineaSuministro
    {
        bool Crear(ClsLineaSuministro dto);

        bool Borrar(ClsLineaSuministro dto);

        List<ClsLineaSuministro> ListarlineaSuministro();
    }
}
