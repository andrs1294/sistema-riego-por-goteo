﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces.Rancheria
{
    public interface IAlarma
    {
        bool Crear(ClsAlarma dto);

        bool Borrar(ClsAlarma dto);

        List<ClsAlarma> ListarAlarmas();
    }
}
