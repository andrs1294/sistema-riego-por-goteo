﻿using DTO.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Interfaces.Rancheria
{
  public  interface IRegistroClima
    {
        List<ClsRegistroClima> ListarClima();
        bool Borrar(ClsRegistroClima dto);
        bool Crear(ClsRegistroClima dto);
    }
}
