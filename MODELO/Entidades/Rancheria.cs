//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MODELO.Entidades
{
    using System;
    using System.Collections.Generic;
    
    public partial class Rancheria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rancheria()
        {
            this.Cultivo = new HashSet<Cultivo>();
            this.Tipo_Nutriente = new HashSet<Tipo_Nutriente>();
        }
    
        public int Pk_Id { get; set; }
        public string Nombre { get; set; }
        public int Fk_Usuario { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cultivo> Cultivo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tipo_Nutriente> Tipo_Nutriente { get; set; }
    }
}
