﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModRegistroClima: IRegistroClima
    {
        public bool Borrar(ClsRegistroClima dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    var clima = db.Registro_Clima.Where(r => r.Pk_Id == dto.id).FirstOrDefault();
                    db.Registro_Clima.Remove(clima);
                    db.SaveChanges();
                    return true;

                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRegistroClima", "Borrar");
                return false;

            }
        }

        public bool Crear(ClsRegistroClima dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Registro_Clima.Where(i => i.Pk_Id == dto.id);
                    if (existe.Any())
                    {
                        return false;
                    }
                    else
                    {
                        Registro_Clima clima = new Registro_Clima();
                        clima.Fecha = dto.fecha;
                        clima.Registro = dto.registro;
                        clima.Fk_Cultivo = dto.fk_cultivo;
                  

                        db.Registro_Clima.Add(clima);

                        db.SaveChanges();

                        return true;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "Crear", "ModRegistroClima");
                return false;
            }
        }
        public List<ClsRegistroClima> ListarClima()
        {
            try
            {
                List<ClsRegistroClima> climas = new List<ClsRegistroClima>();
                using (ERancheria db = new ERancheria())
                {
                    var lista = db.Registro_Clima.ToList();

                    for (int i = 0; i < lista.Count; i++)
                    {
                        ClsRegistroClima clima = new ClsRegistroClima();
                        clima.id = lista[i].Pk_Id;
                        clima.fk_cultivo = lista[i].Fk_Cultivo;
                        clima.registro = lista[i].Registro;
                       


                        climas.Add(clima);
                    }

                }

                return climas;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRegistroClima", "ListarClima");
                return null;
            }
        }
    }
}
