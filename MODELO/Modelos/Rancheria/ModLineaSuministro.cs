﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModLineaSuministro : ILineaSuministro
    {
        public bool Borrar(ClsLineaSuministro dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    var lineaSuministro = db.Linea_Suministro.Where(l => l.Pk_Id == dto.id).FirstOrDefault();
                    db.Linea_Suministro.Remove(lineaSuministro);
                    db.SaveChanges();
                    return true;

                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModLineaSuministro", "Borrar");
                return false;

            }
        }

        public bool Crear(ClsLineaSuministro dto)
        {

            
            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var lista = db.Cultivo_Tanque.Where(t => t.Fk_Cultivo == dto.cultivo).ToList();

                    for (int i = 0; i < lista.Count; i++)
                    {
                        Linea_Suministro lineaSuministro = new Linea_Suministro();

                        lineaSuministro.ListrosXSegundo = dto.litrosPorSegundo;
                        lineaSuministro.Fk_Cultivo = dto.cultivo;
                        lineaSuministro.Fk_Tanque = lista[i].Fk_Tanque;
                        lineaSuministro.LineaObstruida = dto.lineaObstruida;
                        db.Linea_Suministro.Add(lineaSuministro);
                    }
                        
                                        
                    db.SaveChanges();

                    return true;
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "Crear", "ModLineaSuministro");
                return false;
            }
        }

        public List<ClsLineaSuministro> ListarlineaSuministro()
        {
            try
            {
                List<ClsLineaSuministro> lineasSuministro = new List<ClsLineaSuministro>();
                using (ERancheria db = new ERancheria())
                {
                    var lista = db.Linea_Suministro.ToList();

                    for (int i = 0; i < lista.Count; i++)
                    {
                        ClsLineaSuministro linea = new ClsLineaSuministro();

                        linea.id = lista[i].Pk_Id;
                        linea.litrosPorSegundo = lista[i].ListrosXSegundo;
                        linea.cultivo = lista[i].Fk_Cultivo;
                        linea.tanque = lista[i].Fk_Tanque;
                        linea.lineaObstruida = lista[i].LineaObstruida;

                        lineasSuministro.Add(linea);
                    }

                }

                return lineasSuministro;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRancheria", "ListarlineaSuministro");
                return null;
            }
        }
        
    }
}
