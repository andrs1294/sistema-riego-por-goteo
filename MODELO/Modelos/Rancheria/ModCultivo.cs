﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModCultivo : ICultivo
    {
        public bool Borrar(ClsCultivo dto)
        {
            throw new NotImplementedException();
        }

        public int Crear(ClsCultivo dto)
        {

            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Cultivo.Where(i => i.Pk_Id == dto.id);
                    if (existe.Any())
                    {
                        return -1;
                    }
                    else
                    {
                        Entidades.Cultivo cultivo = new Entidades.Cultivo();

                        cultivo.Fk_Rancheria = dto.idRaancheria;
                        cultivo.FechaSiembra = dto.fechaSiembra;
                        cultivo.Ancho = dto.ancho;
                        cultivo.Alto = dto.alto;
                        cultivo.Estado = "ACTIVO";

                        db.Cultivo.Add(cultivo);

                        db.SaveChanges();


                        return cultivo.Pk_Id;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "Crear", "ModCultivo");
                return -1;
            }
        }

        public List<ClsCultivo> ListarCultivo()
        {
            try
            {
                List<ClsCultivo> cultivos = new List<ClsCultivo>();
                using (ERancheria db = new ERancheria())
                {
                    var lista = db.Cultivo.ToList();

                    for (int i = 0; i < lista.Count; i++)
                    {
                        ClsCultivo cultivo = new ClsCultivo();

                        cultivo.id = lista[i].Pk_Id;
                        cultivo.idRaancheria = lista[i].Fk_Rancheria;
                        cultivo.fechaSiembra = lista[i].FechaSiembra;
                        cultivo.ancho = lista[i].Ancho;
                        cultivo.alto = lista[i].Alto;
                        cultivo.Estado = lista[i].Estado;
                        Entidades.Rancheria rancheria = db.Rancheria.Where(r => r.Pk_Id == cultivo.idRaancheria).FirstOrDefault();
                        cultivo.nombre_rancheria = rancheria.Nombre;
                        if (cultivo.Estado.Equals("ACTIVO"))
                        {
                            cultivos.Add(cultivo);
                        }
                        
                        
                    }

                }

                return cultivos;
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModCultivo", "ListarCultivo");
                return null;
            }
        }
        public string GetTipo(int id)
        {

            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Cultivo.Where(i => i.Pk_Id == id);
                    if (existe.Any())
                    {
                        var aux = db.Planta.Where(i => i.Fk_Cultivo == id).FirstOrDefault();
                        return aux.Nombre;
                    }
                    else
                    {
                    return null;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "GetTipo", "ModCultivo");
                return null;

            }
        }
        public string GetTamano(int id)

        {
            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Cultivo.Where(i => i.Pk_Id == id);
                    if (existe.Any())
                    {
                        var aux = db.Cultivo.Where(i => i.Pk_Id == id).FirstOrDefault();
                        return aux.Ancho+","+aux.Alto;
                    }
                    else
                    {



                        return null;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "GetTipo", "ModCultivo");
                return null;

            }
        }
	public bool BorrarCultivo(ClsCultivo dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    var cultivo = db.Cultivo.Where(r => r.Pk_Id == dto.id).FirstOrDefault();
                    cultivo.Estado = "INACTIVO";
                    db.SaveChanges();
                    return true;

                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModRancheria", "BorrarCultivo");
                return false;

            }
        }
	public string GetSiembra(int id) {

            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Cultivo.Where(i => i.Pk_Id == id);
                    if (existe.Any())
                    {
                        var aux = db.Cultivo.Where(i => i.Pk_Id == id).FirstOrDefault();
                        return aux.FechaSiembra+"";
                    }
                    else
                    {



                        return null;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "GetTipo", "ModCultivo");
                return null;

            }

        }
	public string GetCosecha(int id)
        {

            try
            {
                using (ERancheria db = new ERancheria())
                {
                    var existe = db.Cultivo.Where(i => i.Pk_Id == id);
                    if (existe.Any())
                    {
                        var aux = db.Cultivo.Where(i => i.Pk_Id == id).FirstOrDefault();
                        var plata = db.Planta.Where(i => i.Fk_Cultivo == id).FirstOrDefault();
                        if (plata.Nombre.Equals("Tomate"))
                        {
                            DateTime fecha = aux.FechaSiembra;
                            DateTime cosecha=  fecha.AddDays(90);
                            return cosecha+"";

                        }
                        else {
                            DateTime fecha = aux.FechaSiembra;
                          DateTime cosecha=  fecha.AddDays(75);
                            return cosecha + "";

                        }
                    }
                    else
                    {



                        return null;
                    }
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "GetTipo", "ModCultivo");
                return null;

            }

        }

    }
}
