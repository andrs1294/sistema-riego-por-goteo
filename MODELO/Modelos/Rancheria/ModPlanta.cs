﻿using DTO.Rancheria;
using MODELO.Entidades;
using MODELO.Interfaces.Rancheria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO.Modelos.Rancheria
{
    public class ModPlanta : IPlanta
    {
        public bool Crear(ClsPlanta dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {
                    //agregamos tantas plantas como optimamente quepan en el terrero del cultivo
                    var cultivo=db.Cultivo.Where(c => c.Pk_Id == dto.cultivo).FirstOrDefault();
                    int calculo = Calcular(cultivo.Ancho, cultivo.Alto, dto);
                    for (int i = 0; i < calculo; i++)
                    {

                        Planta planta = new Planta();
                        planta.Fk_Cultivo = dto.cultivo;
                        planta.Nombre = dto.nombre;
                        planta.Nutrientes = dto.nutrientes;
                        planta.Tamano = dto.tamano;
                        db.Planta.Add(planta);
                    }
                    db.SaveChanges();

                    return true;
                }
            }

            catch (Exception e)
            {
                Error.INGRESAR(e, "ModPlanta", "Crear");
                return false;
            }

        }
        public int Calcular(int ancho, int alto, ClsPlanta dto)
        {
            //papa
            if (dto.nombre.Equals("Papa"))
            {
                int aux = ancho * alto / 300;     //El Área aproximada que necesita una planta de papa es de 0.3m2, se necesita una
                //distancia entre una planta y otra	de 75 cm y una distancia entre líneas de plantación en el huerto de 38 cm, sin embargo acá esta 
                //multiplicada por un factor de 1000.
                if (aux <= 0)
                {
                    return 10;
                } else if (aux >= 1000)
                {
                    return 1000;
                }
                return aux; //criterio a evaluar la papa
            }
            //tomate
            else if (dto.nombre.Equals("Tomate"))
            {
                int aux = ancho * alto / 500;//El Área aproximada que necesita una planta de tomate es de 0.6m2, se necesita una
                //distancia entre una planta y otra	de 60 cm y una distancia entre líneas de plantación en el huerto de 90 cm, sin embargo acá esta 
                //multiplicada por un factor de 1000.
                if (aux <= 0)
                {
                    return 10;
                }
                else if (aux >= 1000)
                {
                    return 1000;
                }
                return aux;  //criterio a evaluar el tomate


            }
            return 0;
        }

        //Actualizamos los nutrientes y el tamaño de la planta

        public bool Editar(ClsPlanta dto)
        {
            try
            {
                using (ERancheria db = new ERancheria())
                {

                    var planta = db.Planta.Where(r => r.Pk_Id == dto.id).FirstOrDefault();
                    planta.Tamano = dto.tamano;
                    planta.Nutrientes = dto.nutrientes;
                    db.SaveChanges();
                    return true;

                }
            }
            catch (Exception e)
            {
                Error.INGRESAR(e, "ModPlanta", "Editar");
                return false;

            }
        }
    }
}
